import { select } from "d3-selection";
import {scaleBand, scaleLinear, scaleTime, scaleOrdinal } from "d3-scale";
import { schemeAccent, schemeCategory10 } from "d3-scale-chromatic";
import { line } from "d3-shape";
import { malagaStats, avgTemp, TempStat } from "./linechart.data";
import { axisBottom, axisLeft } from "d3-axis";
import { extent } from "d3-array";
import { schemeSet1 } from "d3-scale-chromatic";
import { accessSync } from "fs";

  const d3 = {
      select,
      scaleBand,
      scaleLinear,
      scaleTime,
      schemeSet1,
      extent,
      line,
      axisBottom,
      axisLeft,
      scaleOrdinal,
      schemeAccent,
      schemeCategory10,
    };

  // Tamaño del marco
  const width = 500;
  const height = 300;
  const padding = 50;

  // Meses del año
  const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

  // Creamos la tarjeta.
  const card = d3
    .select("#root")
    .append("div")
      .attr("class", "card");

  // Creamos el 'lienzo' svg.
  const svg = card
    .append("svg")
    .attr("width", "100%")
    .attr("height", "100%")
    .attr("viewBox", `${-padding} ${-padding} ${width + 2 * padding} 
    ${height + 2 * padding}`); // = "0 0 500 300"


// ESTO ESTÁ BIEN
  const xScale = d3
    .scaleBand()
    .domain(months) // Range Jan to Dec 2019
    .range([0, width]); // pixels  

  const yScale = d3.scaleLinear()
    .domain(extent(avgTemp))
    .range([height, 0]);

  const barGroup = svg
    .append('g');

  const colorScale = d3
    .scaleOrdinal(d3.schemeSet1)

// Paint the chart
  barGroup
  .selectAll("path")
  .data(avgTemp)
  .enter()
  .append("rect")
    .attr("x", (d,i) => xScale(months[i]))
    .attr("y", d => yScale(d))
    .attr("width", xScale.bandwidth())
    .attr("height", d => height - yScale(d))
    .attr("fill", "url(#barGradient)");

// Y Axis
barGroup.append('g').call(axisLeft(yScale));

// X Axis
barGroup.append('g')
  .attr('transform', `translate(0, ${height})`)
  .call(axisBottom(xScale));


// OPTIONAL
// Gradiante.
const gradient = svg
  .append("defs")
    .append("linearGradient")
      .attr("id", "barGradient")
      .attr("gradientUnits", "userSpaceOnUse")
      .attr("x1", "0")
      .attr("y1", height)
      .attr("x2", "0")
      .attr("y2", "0");
gradient
  .append("stop")
    .attr("offset", "0")
    .attr("stop-color", "#4743ce");
gradient
  .append("stop")
    .attr("offset", "50%")
    .attr("stop-color", "#ff780a");
gradient
  .append("stop")
    .attr("offset", "100%")
    .attr("stop-color", "#ff0a0a");


// TITULO PARA LOS EJES.

// eje x
const margen= 50;
svg
  .append("text")             
    .attr("transform", `translate(${width/2}, ${height + margen})`)
    .style("text-anchor", "middle")
    .text("Months");

// Título para el eje y
svg
  .append("text")             
    .attr("transform", `rotate(-90)`)
    .attr("y", -margen/2)
    .attr("x", -(height / 2))
    .style("text-anchor", "middle")
    .text("Temperature (℃)");









