#Málaga Average Temperatures

In this web, we have a bar chart and a line chart that contain the Málaga Average Temperatures for a given year.

The bar chart have:

- A vertical bar chart.

- X / Y axis.

- Color scale for bars.

- Axis title.

- A gradient to each temperature.